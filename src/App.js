import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";

class App extends Component {
  state = {
    data: ""
  };

  handleClick = () => {
    fetch("/api/people/1/")
      .then(r => r.json())
      .then(data => {
        this.setState({ data });
      })
      .catch(e => {
        console.error(e);
      });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img
            src={logo}
            onClick={this.handleClick}
            className="App-logo"
            alt="logo"
          />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <pre>{JSON.stringify(this.state.data, undefined, 2)}</pre>
      </div>
    );
  }
}

export default App;
